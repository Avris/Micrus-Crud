<?php
namespace Avris\Micrus\Crud\Config;

final class ListConfig
{
    /** @var array */
    private $fields = [];

    /** @var array */
    private $actions = [];

    /** @var array */
    private $defaultFilters = [];

    /** @var string */
    private $defaultSort = '';

    public function getFields(): array
    {
        return $this->fields;
    }

    public function getActions(): array
    {
        return $this->actions;
    }

    /**
     * @param string $name
     * @param bool|String $key
     * false = just display the value
     * true = display the value as link to its details
     * string = display the value as link to {$key}_show
     * @param bool $sortable
     * @param bool|array|String $filterable
     * true = enable standard filtering
     * false = disable filtering
     * array = filtering using a select box with values of the specified array
     * string = filtering with a select box out of all entities of type specified in $filterable
     * @param string $view
     * @param null|String $label
     * @return $this
     */
    public function add(
        string $name,
        $key = false,
        bool $sortable = true,
        $filterable = true,
        string $view = 'Crud/List/element',
        ?string $label = null
    ): self {
        if (is_string($filterable)) {
            $filterable = ['_model' => $filterable];
        }

        $this->fields[$name] = [
            'label' => $label,
            'key' => $key,
            'sortable' => is_bool($key) ? $sortable : false,
            'filterable' => $filterable,
            'view' => $view,
        ];

        return $this;
    }

    public function addAction(string $template): self
    {
        $this->actions[sha1($template)] = $template;

        return $this;
    }

    public function isFilterable(string $key): bool
    {
        return isset($this->fields[$key]['filterable'])
            && is_array($this->fields[$key]['filterable']);
    }

    public function isFilterableByModel(string $key): bool
    {
        return isset($this->fields[$key]['filterable'])
            && is_array($this->fields[$key]['filterable'])
            && isset($this->fields[$key]['filterable']['_model']);
    }

    public function getDefaultFilters(): array
    {
        return $this->defaultFilters;
    }

    public function setDefaultFilters(array $defaultFilters): self
    {
        $this->defaultFilters = $defaultFilters;

        return $this;
    }

    public function getDefaultSort(): string
    {
        return $this->defaultSort;
    }

    public function setDefaultSort(string $defaultSort): self
    {
        $this->defaultSort = $defaultSort;

        return $this;
    }
}