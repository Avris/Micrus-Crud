<?php
namespace Avris\Micrus\Crud\Config;

final class ShowConfig
{
    /** @var string */
    private $currentSection = '';

    /** @var array */
    private $fields = [];

    public function getFields(): array
    {
        return $this->fields;
    }

    public function startSection(string $section): self
    {
        $this->currentSection = $section;

        return $this;
    }

    public function endSection(): self
    {
        $this->currentSection = '';

        return $this;
    }

    /**
     * @param string $name
     * @param bool|String $key
     * false = just display the value
     * true = display the value as link to its details
     * string = display the value as link to {$key}_show
     * @param bool $raw
     * @param string $view
     * @param null|String $label
     * @return $this
     */
    public function add($name, $key = false, $raw = false, $view = 'Crud/Show/element', $label = null): self
    {
        if (!isset($this->fields[$this->currentSection])) {
            $this->fields[$this->currentSection] = [];
        }

        $this->fields[$this->currentSection][$name] = [
            'label' => $label,
            'raw' => $raw,
            'key' => $key,
            'view' => $view,
        ];

        return $this;
    }

}