<?php
namespace Avris\Micrus\Crud\Config;

final class ExportConfig
{
    /** @var array */
    private $fields = [];

    public function getFields(): array
    {
        return $this->fields;
    }

    public function add(string $name, callable $format = null): self
    {
        $this->fields[$name] = $format;

        return $this;
    }
}
