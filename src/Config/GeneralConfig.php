<?php
namespace Avris\Micrus\Crud\Config;

final class GeneralConfig
{
    /** @var string[] */
    private $actions = [];

    public function getActions(): array
    {
        return $this->actions;
    }

    public function addAction(string $template)
    {
        $this->actions[] = $template;

        return $this;
    }
}
