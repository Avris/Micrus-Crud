<?php
namespace Avris\Micrus\Crud\Config;

final class ConfigBag
{
    /** @var ListConfig */
    private $list;

    /** @var ShowConfig */
    private $show;

    /** @var GeneralConfig */
    private $general;

    /** @var ExportConfig */
    private $export;

    public function __construct()
    {
        $this->list = new ListConfig();
        $this->show = new ShowConfig();
        $this->general = new GeneralConfig();
        $this->export = new ExportConfig();
    }

    public function getList(): ListConfig
    {
        return $this->list;
    }

    public function getShow(): ShowConfig
    {
        return $this->show;
    }

    public function getGeneral(): GeneralConfig
    {
        return $this->general;
    }

    public function getExport(): ExportConfig
    {
        return $this->export;
    }
}
