<?php
namespace Avris\Micrus\Crud\Controller;

use Avris\Container\ContainerInterface;
use Avris\Http\Response\ResponseInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Crud\CrudLoader;
use Avris\Micrus\Crud\Metric\Metric;
use Avris\Micrus\Crud\ORM\DoctrineFinder;
use Avris\Micrus\Exception\NotFoundException;
use Doctrine\ORM\EntityManagerInterface;

class DashboardController extends Controller
{
    /** @var CrudLoader */
    private $crudLoader;

    public function __construct(ContainerInterface $container, CrudLoader $crudLoader)
    {
        parent::__construct($container);
        $this->crudLoader = $crudLoader;
    }

    public function dashboardAction(): ResponseInterface
    {
        $cruds = [];

        foreach ($this->crudLoader->getCruds() as $route => $crudModel) {
            $metrics = [];

            foreach ($crudModel->getMetrics() as $name => $metricOptions) {
                $metrics[$name] = $this->buildMetric($metricOptions, $crudModel->getClass());
            }

            $cruds[$route] = [
                'name' => $crudModel->getName(),
                'model' => $crudModel->getClass(),
                'icon' => $crudModel->getIcon(),
                'metrics' => $metrics,
            ];
        }

        return $this->render(['crudsWithMetrics' => $cruds], 'Crud/Dashboard/dashboard');
    }

    public function metricApplyAction(
        SessionInterface $session,
        string $crudName,
        string $metric
    ): ResponseInterface {
        foreach ($this->crudLoader->getCruds() as $route => $crudModel) {
            if ($crudModel->getName() !== $crudName || !($metricOptions = $crudModel->getMetric($metric))) {
                continue;
            }

            $metric = $this->buildMetric($metricOptions, $crudModel->getClass());

            $plainFilters = $metric->apply($crudModel);

            $session->set('_' . $crudModel->getName() . '_filter', $plainFilters);

            return $this->redirectToRoute('admin_' . $crudModel->getName(). '_list');
        }

        throw new NotFoundException;
    }

    private function buildMetric(array $metricOptions, string $model): Metric
    {
        return $this->container->get($metricOptions['type'])->initialise($model, $metricOptions);
    }
}
