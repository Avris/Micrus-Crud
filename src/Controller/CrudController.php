<?php
namespace Avris\Micrus\Crud\Controller;

use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Controller\Controller;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Crud\Config\ConfigBag;
use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Micrus\Crud\Config\GeneralConfig;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Config\ShowConfig;
use Avris\Micrus\Crud\CrudLoader;
use Avris\Micrus\Crud\Model\CrudModel;
use Avris\Micrus\Crud\Exporter\AbstractExporter;
use Avris\Micrus\Crud\ListRequestHandler;
use Avris\Micrus\Crud\ORM\DoctrineFinder;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Container\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;

abstract class CrudController extends Controller
{
    /** @var CrudModel */
    protected $crudModel;

    /** @var ConfigBag */
    protected $config;

    /** @var AbstractExporter[] */
    private $exporters = [];

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        ContainerInterface $container,
        RequestInterface $request,
        CrudLoader $crudLoader,
        array $crudExporters,
        EntityManagerInterface $em
    ) {
        parent::__construct($container);

        $this->crudModel = $crudLoader->getCruds()->get($request->getQuery()->get('_crud'));

        $this->config = new ConfigBag();
        $this->configureGeneral($this->config->getGeneral());

        /** @var AbstractExporter $exporter */
        foreach ($crudExporters as $exporter) {
            $this->exporters[$exporter->getExtension()] = $exporter;
        }

        $this->em = $em;
    }

    public function listAction(
        RequestInterface $request,
        SessionInterface $session,
        DoctrineFinder $finder,
        ListRequestHandler $handler,
        $format = null
    ): ResponseInterface {
        $this->configureList($this->config->getList());
        $this->configureExport($this->config->getExport());

        $handler->handle($this->config->getList(), $request, $session, $this->crudModel);

        if ($format) {
            $exporter = $this->exporters[$format] ?? null;
            if (!$exporter || !$this->crudModel->supportsExporter($format)) {
                throw new NotFoundHttpException(sprintf('Exporter for the "%s" format is not supported'), $format);
            }

            return $exporter->buildResponse(
                $this->crudModel->getName(),
                $finder->find(
                    $this->crudModel->getClass(),
                    $handler->getFilters(),
                    $handler->getSortBy(),
                    $handler->getSortDir()
                ),
                $this->config->getExport()
            );
        }

        return $this->buildResponse('list', [
            'entities' => $finder->find(
                $this->crudModel->getClass(),
                $handler->getFilters(),
                $handler->getSortBy(),
                $handler->getSortDir(),
                $handler->getCurrentPage(),
                $handler->getPerPage()
            ),
            'count' => $finder->count($this->crudModel->getClass(), $handler->getFilters()),
            'finder' => $finder,
            'page' => $handler->getCurrentPage(),
            'perPage' => $handler->getPerPage(),
            'sort' => [
                'plain' => $handler->getSortPlain(),
                'by' => $handler->getSortBy(),
                'dir' => $handler->getSortDir(),
            ],
            'filter' => $handler->getFilterPlain(),
            'exportFormats' => $this->crudModel->getExporters(),
        ]);
    }

    public function newAction(RequestInterface $request): ResponseInterface
    {
        $entity = $this->create();

        $form = $this->form($this->getNewForm($entity), $entity, $request);

        if ($this->handleForm($form, $entity)) {
            $entity = $form->buildObject();
            $this->preCreate($entity);
            $this->em->persist($entity);
            $this->postCreate($entity);
            $this->em->flush();
            $this->addFlash('success', l('crud:new.done'));

            return $this->redirectToSubroute($request, ['id' => $entity->getId()]);
        }

        return $this->buildResponse('new', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    public function editAction(RequestInterface $request, $id): ResponseInterface
    {
        $entity = $this->fetchEntity($id);

        $form = $this->form($this->getEditForm($entity), $entity, $request);

        if ($this->handleForm($form, $entity)) {
            $entity = $form->buildObject();
            $this->preUpdate($entity);
            $this->em->persist($entity);
            $this->postUpdate($entity);
            $this->em->flush();
            $this->addFlash('success', l('crud:edit.done'));

            return $this->redirectToSubroute($request, ['id' => $entity->getId()]);
        }

        return $this->buildResponse('edit', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    protected function fetchEntity($id)
    {
        return $this->em->getRepository($this->crudModel->getClass())->find($id);
    }

    protected function redirectToSubroute(RequestInterface $request, array $params = []): ResponseInterface
    {
        /** @var RouterInterface $router */
        $router = $this->get(RouterInterface::class);

        foreach ([$request->getData()->get('_after'), 'edit', 'show', 'list'] as $subRoute) {
            if (!$subRoute) {
                continue;
            }

            $route = 'admin_' . $this->crudModel->getName() . '_' . $subRoute;

            if ($router->routeExists($route)) {
                return $this->redirectToRoute($route, $params);
            }
        }

        return $this->redirectToRoute($router->getDefaultRoute(), $params);
    }

    public function showAction($id): ResponseInterface
    {
        $entity = $this->fetchEntity($id);

        $this->configureShow($this->config->getShow());

        return $this->buildResponse('show', [
            'entity' => $entity,
        ]);
    }

    public function deleteAction(RequestInterface $request, $id): ResponseInterface
    {
        $entity = $this->fetchEntity($id);

        if (in_array($request->getMethod(), ['POST','DELETE'])) {
            $this->preDelete($entity);
            $this->em->remove($entity);
            $this->postDelete($entity);
            $this->em->flush();
            $this->addFlash('success', l('crud:delete.done'));

            return $this->redirectToRoute('admin_' . $this->crudModel->getName() . '_list');
        }

        return $this->buildResponse('delete', [
            'entity' => $entity,
        ]);
    }

    protected function buildResponse($action, array $vars): ResponseInterface
    {
        return $this->render(
            array_merge($vars, $this->viewVars(), [
                'action' => $action,
                'model' => $this->crudModel,
                'config' => $this->config,
            ]),
            sprintf('Crud/%s/%s', ucfirst($action), $action)
        );
    }

    protected function configureShow(ShowConfig $config)
    {
        $config->add('__toString', true);
        $config->add('id', 'ID');
    }

    protected function configureList(ListConfig $config)
    {
        $config->add('__toString', true, true, false, false);
    }

    protected function configureGeneral(GeneralConfig $config)
    {

    }

    protected function configureExport(ExportConfig $config)
    {

    }

    protected function create()
    {
        $class = $this->crudModel->getClass();

        return new $class;
    }

    protected function getNewForm($entity): string
    {
        return $this->getForm($entity);
    }

    protected function getEditForm($entity): string
    {
        return $this->getForm($entity);
    }

    protected function getForm($entity): string
    {
        return $this->crudModel->getForm();
    }

    protected function preCreate($entity)
    {

    }

    protected function postCreate($entity)
    {

    }

    protected function preUpdate($entity)
    {

    }

    protected function postUpdate($entity)
    {

    }

    protected function preDelete($entity)
    {

    }

    protected function postDelete($entity)
    {

    }

    protected function viewVars()
    {
        return [];
    }
}