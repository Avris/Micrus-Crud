<?php
namespace Avris\Micrus\Crud\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
final class Crud
{
    /** @var string */
    private $model;

    /** @var string */
    private $form;

    /** @var string */
    private $icon;

    /** @var array */
    private $metrics;

    /** @var int */
    private $perPage;

    /** @var string[] */
    private $addRoutes;

    /** @var string[] */
    private $disableRoutes;

    public function __construct($values)
    {
        $this->model = $values['value'] ?? $values['model'];
        $this->form = $values['form'] ?? null;
        $this->icon = $values['icon'] ?? null;
        $this->metrics = $values['metrics'] ?? [];
        $this->perPage = $values['perPage'] ?? null;
        $this->addRoutes = $values['addRoutes'] ?? [];
        $this->disableRoutes = $values['disableRoutes'] ?? [];
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function getForm(): string
    {
        return $this->form;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getMetrics(): array
    {
        return $this->metrics;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return string[]
     */
    public function getAddRoutes(): array
    {
        return $this->addRoutes;
    }

    /**
     * @return string[]
     */
    public function getDisableRoutes(): array
    {
        return $this->disableRoutes;
    }

    public function toArray(): array
    {
        return [
            'model' => $this->model,
            'form' => $this->form,
            'icon' => $this->icon,
            'metrics' => array_map(function (CrudMetric $metric) { return $metric->toArray(); }, $this->metrics),
            'perPage' => $this->perPage,
            'addRoutes' => $this->addRoutes,
            'disableRoutes' => $this->disableRoutes,
        ];
    }
}
