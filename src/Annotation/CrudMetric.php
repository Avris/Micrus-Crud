<?php
namespace Avris\Micrus\Crud\Annotation;

/**
 * @Annotation
 * @Target({"ANNOTATION"})
 */
final class CrudMetric
{
    /** @var string */
    private $type;

    /** @var array */
    private $filters;

    /** @var string */
    private $relation;

    public function __construct($values)
    {
        $this->type = $values['value'] ?? $values['type'];
        $this->filters = $values['filters'] ?? [];
        $this->relation = $values['relation'] ?? null;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getRelation(): string
    {
        return $this->relation;
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'filters' => $this->filters,
            'relation' => $this->relation,
        ];
    }
}
