<?php
namespace Avris\Micrus\Crud\Annotation;

use Avris\Micrus\Annotations\Loader\AnnotationsLoaderInterface;
use Avris\Micrus\Crud\Annotation;

final class CrudAnnotationsLoader implements AnnotationsLoaderInterface
{
    /** @var array */
    private $config = [];

    public function getDir(): string
    {
        return 'Controller';
    }

    public function loadClass(\ReflectionClass $class, $annotation)
    {
        if (!$annotation instanceof Annotation\Crud) {
            return;
        }

        $this->config[$class->getName()] = $annotation->toArray();
    }

    public function loadMethod(\ReflectionMethod $method, $annotation)
    {
    }

    public function loadProperty(\ReflectionProperty $property, $annotation)
    {
    }

    public function get(): array
    {
        return [
            'cruds' => $this->config,
        ];
    }
}
