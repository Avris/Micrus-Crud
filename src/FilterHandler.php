<?php
namespace Avris\Micrus\Crud;

use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Exception\InvalidArgumentException;

class FilterHandler
{
    public function buildFiltering(array $filterPlain, ListConfig $listConfig = null): array
    {
        if (empty($filterPlain)) {
            return [];
        }

        $filters = [];

        foreach ($filterPlain as $key => $filterValue) {
            if (empty($filterValue)) {
                continue;
            }

            if (!preg_match('/[A-Za-z-_]+/', $key)) {
                throw new InvalidArgumentException('Incorrect column name');
            }

            if (in_array($filterValue, ['~', '!~'])) {
                $filters[$key] = $filterValue;
                continue;
            }

            if ($listConfig && $listConfig->isFilterableByModel($key)) {
                $filters[$key . '_id'] = ['=', $filterValue];
                continue;
            }

            list($operator, $value) = $this->extractOperator($filterValue, 2, ['<=', '>=', '<>']);
            if (!$operator) {
                list($operator, $value) = $this->extractOperator($filterValue, 1, ['<', '>', '=']);
            }

            if (empty($value)) {
                continue;
            }

            $value = trim($value);
            if (!$operator) {
                if ($listConfig && $listConfig->isFilterable($key)) {
                    $operator = '=';
                    $value = $filterValue;
                } else {
                    $operator = 'LIKE';
                    $value = '%'.$filterValue.'%';
                }
            }

            $filters[$key] = [$operator, is_numeric($value) ? (double) $value : $value];
        }

        return $filters;
    }

    protected function extractOperator($string, $length, array $operators)
    {
        if (in_array(substr($string, 0, $length), $operators)) {
            return [
                substr($string, 0, $length),
                substr($string, $length),
            ];
        }

        return [null, '---'];
    }
}
