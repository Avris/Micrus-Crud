<?php
namespace Avris\Micrus\Crud\Metric;

use Avris\Micrus\Crud\Model\CrudModel;
use Avris\Micrus\Crud\FilterHandler;
use Avris\Micrus\Crud\ORM\DoctrineFinder;

abstract class Metric
{
    /** @var DoctrineFinder */
    protected $finder;

    /** @var FilterHandler */
    private $filterHandler;

    /** @var string */
    protected $model;

    /** @var array */
    private $options;

    public function __construct(
        DoctrineFinder $finder,
        FilterHandler $filterHandler
    ) {
        $this->finder = $finder;
        $this->filterHandler = $filterHandler;
    }

    public function initialise(string $model, array $options = []): self
    {
        $this->model = $model;
        $this->options = $options;

        return $this;
    }

    protected function getOption($name, $default = null)
    {
        return $this->options[$name] ?? $default;
    }

    abstract public function getTemplate(): string;

    abstract public function getValue(): int;

    public function apply(CrudModel $crudModel): array
    {
        return [];
    }

    public function hasLink(): bool
    {
        return false;
    }

    protected function getFilters(): array
    {
        return $this->filterHandler->buildFiltering($this->getOption('filters', []));
    }
}
