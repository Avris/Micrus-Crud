<?php
namespace Avris\Micrus\Crud\Metric;

use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Crud\Model\CrudModel;

class SumMetric extends Metric
{
    public function getTemplate(): string
    {
        return 'simple';
    }

    public function getValue(): int
    {
        $result = $this->finder->customQuery(
            $this->model,
            $this->getFilters(),
            ['sum(t.'.$this->getOption('field').')']
        );

        $row = reset($result);

        return reset($row);
    }
}
