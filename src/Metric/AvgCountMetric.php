<?php
namespace Avris\Micrus\Crud\Metric;

use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Crud\Model\CrudModel;

class AvgCountMetric extends Metric
{
    public function getTemplate(): string
    {
        return 'simple';
    }

    public function getValue(): int
    {
        $result = $this->finder->customQuery(
            $this->model,
            $this->getFilters(),
            ['count(u.id) as cc'],
            ['t.' . $this->getOption('relation') => 'u'],
            ['t.id']
        );

        $sum = array_reduce($result, function ($carry, $row) {
            return $carry + $row['cc'];
        });

        $avg = count($result)
            ? $sum / count($result)
            : 0;

        return number_format($avg, $this->getOption('decimals', 2));
    }
}
