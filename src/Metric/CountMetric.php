<?php
namespace Avris\Micrus\Crud\Metric;

use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Crud\Model\CrudModel;

class CountMetric extends Metric
{
    public function getTemplate(): string
    {
        return 'simple';
    }

    public function getValue(): int
    {
        return $this->finder->count($this->model, $this->getFilters());
    }

    public function apply(CrudModel $crudModel): array
    {
        $filters = $this->getFilters();

        $plainFilters = [];

        foreach ($filters as $name => $filter) {
            $plainFilters[$name] = is_array($filter) ? $filter[0] . $filter[1] : $filter;
        }

        return $plainFilters;
    }

    public function hasLink(): bool
    {
        return true;
    }
}
