<?php
namespace Avris\Micrus\Crud\Metric;

use Avris\Container\ContainerInterface;
use Avris\Http\Response\Response;
use Avris\Http\Response\ResponseInterface;
use Avris\Micrus\Crud\Model\CrudModel;

class AvgMetric extends Metric
{
    public function getTemplate(): string
    {
        return 'simple';
    }

    public function getValue(): int
    {
        $result = $this->finder->customQuery(
            $this->model,
            $this->getFilters(),
            ['avg(t.'.$this->getOption('field').')']
        );

        $row = reset($result);
        $avg = reset($row);

        return number_format($avg, $this->getOption('decimals', 2));
    }
}
