<?php
namespace Avris\Micrus\Crud\Exporter;

use Avris\Micrus\Crud\Config\ExportConfig;

class CsvExporter extends AbstractExporter
{
    public function export(string $model, iterable $entities, ExportConfig $config)
    {
        $output = fopen('php://output', 'w');

        fputcsv($output, array_merge(['id'], array_keys($config->getFields())), ';');

        foreach ($entities as $id => $entity) {
            fputcsv($output, array_merge([$id], $this->convertEntity($entity, $config)), ';');
        }
    }

    public function getExtension(): string
    {
        return 'csv';
    }

    public function getMime(): string
    {
        return 'text/comma-separated-values';
    }
}
