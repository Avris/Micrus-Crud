<?php
namespace Avris\Micrus\Crud\Exporter;

use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Forms\Accessor;

class XmlExporter extends AbstractExporter
{
    public function export(string $model, iterable $entities, ExportConfig $config)
    {
        echo '<?xml version="1.0"?>' . PHP_EOL;
        echo '<'.$model.'List>' . PHP_EOL;

        foreach ($entities as $id => $entity) {
            $element = new \SimpleXMLElement('<'.$model.'/>');
            $element->addAttribute('id', $id);
            foreach ($this->convertEntity($entity, $config) as $key => $value) {
                $element->addChild($key, $value);
            }
            echo str_replace('<?xml version="1.0"?>' . PHP_EOL, '', $element->asXML());
        }

        echo '</'.$model.'List>' . PHP_EOL;
    }

    public function getExtension(): string
    {
        return 'xml';
    }

    public function getMime(): string
    {
        return 'text/xml';
    }
}
