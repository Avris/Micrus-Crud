<?php
namespace Avris\Micrus\Crud\Exporter;

use Avris\Micrus\Crud\Config\ExportConfig;

class JsonExporter extends AbstractExporter
{
    public function export(string $model, iterable $entities, ExportConfig $config)
    {
        echo '{';

        $separator = '';
        foreach ($entities as $id => $entity) {
            echo $separator . json_encode($id) . ':' . json_encode($this->convertEntity($entity, $config));
            $separator = ',';
        }

        echo '}';
    }

    public function getExtension(): string
    {
        return 'json';
    }

    public function getMime(): string
    {
        return 'application/json';
    }
}
