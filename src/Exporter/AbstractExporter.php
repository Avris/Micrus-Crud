<?php
namespace Avris\Micrus\Crud\Exporter;

use Avris\Bag\BagHelper;
use Avris\Http\Response\StreamedResponse;
use Avris\Micrus\Crud\Config\ExportConfig;
use Avris\Forms\Accessor;

abstract class AbstractExporter
{
    public function buildResponse(string $model, iterable $entities, ExportConfig $config): StreamedResponse
    {
        return new StreamedResponse(
            function () use ($model, $entities, $config) {
                $this->export($model, $entities, $config);
            },
            200,
            [
                'Content-Type' => $this->getMime(),
                'Content-Disposition' => 'attachment; filename="'.addslashes($model . '.' . $this->getExtension()).'"',
                'Pragma' => 'public',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            ]
        );
    }

    abstract public function export(string $model, iterable $entities, ExportConfig $config);

    abstract public function getExtension(): string;

    abstract public function getMime(): string;

    /**
     * @return string[]
     */
    final protected function convertEntity($entity, ExportConfig $config): array
    {
        $entity = new Accessor($entity);

        $fields = [];

        foreach ($config->getFields() as $field => $callback) {
            if ($callback === null) {
                $callback = function (Accessor $entity, $field) {
                    $value = $entity->{$field};

                    if ($value instanceof \DateTime) {
                        return $value->format(l('format.datetime'));
                    }

                    return BagHelper::isArray($value)
                        ? implode(', ', BagHelper::toArray($value))
                        : (string) $value;
                };
            }

            $fields[$field] = $callback($entity, $field);
        }

        return $fields;
    }
}
