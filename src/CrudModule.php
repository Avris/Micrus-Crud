<?php
namespace Avris\Micrus\Crud;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

final class CrudModule implements ModuleInterface
{
    use ModuleTrait;
}
