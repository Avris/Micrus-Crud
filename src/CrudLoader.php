<?php
namespace Avris\Micrus\Crud;

use Avris\Micrus\Annotations\AnnotationsLoader;
use Avris\Micrus\Bootstrap\ConfigExtension;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\Routing\Service\RouteParser;
use Avris\Micrus\Crud\Annotation\CrudAnnotationsLoader;
use Avris\Micrus\Crud\Controller\DashboardController;
use Avris\Micrus\Crud\Exporter\AbstractExporter;
use Avris\Micrus\Crud\Model\CrudModel;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Bag\Bag;
use Avris\Micrus\Tool\Cache\CacherInterface;

class CrudLoader implements ConfigExtension
{
    const DEFAULT_ROUTES = [
        'list' => '/list.{format=}',
        'new' => '/new',
        'edit' => '/{__restr__:id}/edit',
        'show' => '/{__restr__:id}/show',
        'delete' => '/{__restr__:id}/delete',
    ];

    /** @var RouteParser */
    private $routeParser;

    /** @var string */
    private $base;

    /** @var string */
    private $restriction;

    /** @var array */
    private $config;

    /** @var Bag */
    private $cruds = [];

    /** @var string[] */
    private $exporterFormats = [];

    public function __construct(
        CacherInterface $cacher,
        RouteParser $routeParser,
        Bag $configCrud,
        AnnotationsLoader $loader,
        array $crudExporters
    ) {
        $this->routeParser = $routeParser;
        $this->base = '/'. trim($configCrud->get('baseRoute'), '/');
        $this->restriction = $configCrud->get('idRestriction');
        $this->exporterFormats = array_map(function (AbstractExporter $exporter) {
            return $exporter->getExtension();
        }, $crudExporters);

        list($this->cruds, $this->config) = $cacher->cache(
            'crud',
            function () use ($loader, $configCrud) {
                $config = $loader->extend($configCrud, CrudAnnotationsLoader::class);

                $cruds = $this->loadCruds($config);

                $exportedConfig = [
                    'routing' => $this->extractRoutes($config->get('dashboard'), $cruds),
                    'security' => [
                        'restrictions' => $config->get('restrictions', [
                            'adminPanel' => [ 'pattern' => '^(/[a-z][a-z](_[A-Z][A-Z])?)?'.$this->base, 'roles' => ['ROLE_ADMIN'] ]
                        ]),
                    ],
                ];

                return [$cruds, $exportedConfig];
            }
        );
    }

    private function loadCruds(Bag $config)
    {
        $cruds = new Bag();

        foreach($config->get('cruds') as $controller => $options) {
            if (!isset($options['model'])) {
                throw new InvalidArgumentException(sprintf(
                    'Parameter "model" is required for crud entry "%s"',
                    $controller
                ));
            }

            $class = $options['model'];
            $name = $this->extractName($class);

            $cruds[$class] = new CrudModel(
                $class,
                $name,
                $options['form'] ?? null,
                $options['icon'] ?? null,
                $this->buildRoutes($options, $name, $controller),
                $options['perPage'] ?? $config->get('perPage'),
                $options['exporters'] ?? $this->exporterFormats,
                $options['metrics'] ?? []
            );
        }

        return $cruds;
    }

    private function extractName(string $class)
    {
        preg_match('#\\\\([^\\\\]+)$#Uui', $class, $matches);

        return $matches[1] ?? $class;
    }

    private function buildRoutes(array $options, string $name, string $controller): array
    {
        $routes = [];
        foreach ($this->buildRouteDefinitions($options) as $action => $pattern) {
            $routes[$action] = $this->routeParser->parse(
                sprintf('admin_%s_%s', $name, $action),
                sprintf(
                    '%s -> %s/%s %s',
                    rtrim(str_replace('__restr__', $this->restriction, $pattern), '/'),
                    $controller,
                    $action,
                    json_encode([
                        'defaults' => [
                            '_crud' => $options['model'],
                        ],
                    ])
                ),
                $this->base . '/' . ($options['path'] ?? lcfirst($name))
            );
        }

        return $routes;
    }

    private function buildRouteDefinitions(array $options)
    {
        $routes = static::DEFAULT_ROUTES;

        if (isset($options['disableRoutes'])) {
            foreach ($options['disableRoutes'] as $disabledRoute) {
                unset($routes[$disabledRoute]);
            }
        }

        if (isset($options['addRoutes'])) {
            foreach ($options['addRoutes'] as $key => $value) {
                $routes[$key] = $value;
            }
        }

        return $routes;
    }

    /**
     * @param bool $dashboardEnabled
     * @param Bag|CrudModel[] $cruds
     * @return Route[]
     */
    private function extractRoutes(bool $dashboardEnabled, Bag $cruds)
    {
        $routes = [];
        if ($dashboardEnabled) {
            $routes['admin_dashboard'] = new Route(
                'admin_dashboard',
                $this->base,
                DashboardController::class . '/dashboard'
            );

            $routes['admin_metricApply'] = new Route(
                'admin_metricApply',
                $this->base . '/metric-apply/{crud}/{metric}',
                DashboardController::class . '/metricApply'
            );
        }

        foreach ($cruds as $crud) {
            foreach ($crud->getRoutes() as $route) {
                $routes[$route->getName()] = $route;
            }
        }

        return $routes;
    }

    /**
     * @return Bag|CrudModel[]
     */
    public function getCruds()
    {
        return $this->cruds;
    }

    public function extendConfig(): array
    {
        return $this->config;
    }
}
