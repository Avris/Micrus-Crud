<?php
namespace Avris\Micrus\Crud\Model;

use Avris\Micrus\Controller\Routing\Model\Route;

class CrudModel
{
    /** @var string */
    private $class;

    /** @var string */
    private $name;

    /** @var string */
    private $form;

    /** @var ?string */
    private $icon;

    /** @var Route[] */
    private $routes;

    /** @var int */
    private $perPage;

    /** @var string[] */
    private $exporters;

    /** @var array[] */
    private $metrics;

    public function __construct(
        string $class,
        string $name,
        ?string $form,
        ?string $icon,
        array $routes,
        int $perPage,
        array $exporters,
        array $metrics
    ) {
        $this->class = $class;
        $this->name = $name;
        $this->form = $form;
        $this->icon = $icon;
        $this->routes = $routes;
        $this->perPage = $perPage;
        $this->exporters = $exporters;
        $this->metrics = $metrics;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getForm(): string
    {
        return $this->form;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @return Route[]
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @return string[]
     */
    public function getExporters(): array
    {
        return $this->exporters;
    }

    public function supportsExporter(string $format): bool
    {
        return in_array($format, $this->exporters, true);
    }

    public function getMetrics(): array
    {
        return $this->metrics;
    }

    public function getMetric(string $name): ?array
    {
        return $this->metrics[$name] ?? null;
    }
}
