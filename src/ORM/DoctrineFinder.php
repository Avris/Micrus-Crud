<?php
namespace Avris\Micrus\Crud\ORM;

use Avris\Bag\BagHelper;
use Avris\Forms\Accessor;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

final class DoctrineFinder
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function find(
        string $model,
        array $filters,
        string $sortBy,
        string $sortDir,
        int $currentPage = null,
        int $perPage = null
    ) {
        $qb = $this->buildBasicQuery($model, $filters);

        $qb->orderBy('t.' . $sortBy, $sortDir);

        if ($currentPage !== null) {
            $qb->setFirstResult($perPage * ($currentPage - 1));
            $qb->setMaxResults($perPage);
        }

        foreach ($qb->getQuery()->iterate() as $row) {
            yield Accessor::get($row[0], 'id') => $row[0];
        }
    }

    public function findAll(string $model): array
    {
        $items = [];
        foreach ($this->buildBasicQuery($model, [])->getQuery()->getResult() as $object) {
            $items[Accessor::get($object, 'id')] = $object;
        }

        return $items;
    }

    public function count(string $model, array $filters): int
    {
        $qb = $this->buildBasicQuery($model, $filters);

        $qb->select('count(t.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function customQuery(
        string $model,
        array $filters,
        array $selects = [],
        array $joins = [],
        array $groups = []
    ): array {
        $qb = $this->buildBasicQuery($model, $filters);

        foreach (BagHelper::toArray($selects) as $select) {
            $qb->select($select);
        }

        foreach (BagHelper::toArray($joins) as $join => $alias) {
            $qb->join($join, $alias);
        }

        foreach (BagHelper::toArray($groups) as $group) {
            $qb->groupBy($group);
        }

        return $qb->getQuery()->getScalarResult();
    }

    private function buildBasicQuery(string $model,array $filters): QueryBuilder
    {
        $qb = $this->em->getRepository($model)->createQueryBuilder('t');

        foreach ($filters as $column => $filter) {
            if (preg_match('#^(.*)_id$#i', $column, $matches)) {
                $column = $matches[1];
            }

            if ($filter === '~') {
                $qb->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->eq('t.' . $column, ':' . $column),
                        $qb->expr()->isNull('t.' . $column)
                    )
                );
                $qb->setParameter($column, '');
                continue;
            }

            if ($filter === '!~') {
                $qb->andWhere(
                    $qb->expr()->andX(
                        $qb->expr()->not($qb->expr()->eq('t.' . $column, ':' . $column)),
                        $qb->expr()->isNotNull('t.' . $column)
                    )
                );
                $qb->setParameter($column, '');
                continue;
            }

            list($operator, $value) = $filter;
            if (substr($value, 0, 1) === '@') {
                $value = (new \DateTime(substr($value, 1)))->format('Y-m-d H:i:s');
            }

            $qb->andWhere(sprintf('t.%s %s :%s', $column, $operator, $column));
            $qb->setParameter($column, $value);
        }

        return $qb;
    }
}
