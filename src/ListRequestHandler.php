<?php
namespace Avris\Micrus\Crud;

use Avris\Bag\BagHelper;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Crud\Config\ListConfig;
use Avris\Micrus\Crud\Model\CrudModel;

final class ListRequestHandler
{
    /** @var FilterHandler */
    private $filterHandler;

    /** @var int */
    private $currentPage;

    /** @var int */
    private $perPage;

    /** @var string|null */
    private $sortPlain;

    /** @var string */
    private $sortBy = 'id';

    /** @var string asc|desc */
    private $sortDir = 'desc';

    /** @var array */
    private $filterPlain;

    /** @var array */
    private $filters;

    public function __construct(FilterHandler $filterHandler)
    {
        $this->filterHandler = $filterHandler;
    }

    public function handle(ListConfig $config, RequestInterface $request, SessionInterface $session, CrudModel $crud)
    {
        $this->currentPage = $request->getQuery()->get('page', 1);
        $this->perPage = $crud->getPerPage();

        $this->buildSorting($request, $config->getDefaultSort());

        $this->filterPlain = $this->loadFilterPlain($request, $session, $crud->getName())
            + $config->getDefaultFilters();

        $this->filters = $this->filterHandler->buildFiltering($this->filterPlain, $config);
    }

    private function buildSorting(RequestInterface $request, string $default = '')
    {
        $sort = $request->getQuery()->get('sort') ?: $default;

        if (!$sort) {
            return;
        }

        list($sortBy, $sortDir) = explode('|', $sort) + [0 => 'id', 1 => 'asc'];
        $sortDir = strtolower($sortDir);
        if (preg_match('/[A-Za-z-_]+/', $sortBy) && in_array($sortDir, ['asc', 'desc'])) {
            $this->sortPlain = $sort;
            $this->sortBy = $sortBy;
            $this->sortDir = $sortDir;
        }
    }

    private function loadFilterPlain(RequestInterface $request, SessionInterface $session, string $crudName): array
    {
        if ($request->getQuery()->get('clear')) {
            $session->set('_' . $crudName . '_filter', null);
            return [];
        }

        if ($request->isPost()) {
            $filter = $request->getData()->get($crudName . '_filter');
            $session->set('_' . $crudName . '_filter', $filter);
        } else {
            $filter = $session->get('_' . $crudName . '_filter');
        }

        return BagHelper::toArray($filter);
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getSortPlain(): ?string
    {
        return $this->sortPlain;
    }

    public function getSortBy(): string
    {
        return $this->sortBy;
    }

    public function getSortDir(): string
    {
        return $this->sortDir;
    }

    public function getFilterPlain(): array
    {
        return $this->filterPlain;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }
}
