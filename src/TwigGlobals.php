<?php
namespace Avris\Micrus\Crud;

use Twig\TwigFunction;

class TwigGlobals extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /** @var CrudLoader */
    protected $crud;

    public function __construct(CrudLoader $crud)
    {
        $this->crud = $crud;
    }

    public function getGlobals()
    {
        return [
            'cruds' => $this->crud->getCruds(),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('startBreadcrumbs', function () {
                return new \ArrayObject();
            }),
        ];
    }
}
